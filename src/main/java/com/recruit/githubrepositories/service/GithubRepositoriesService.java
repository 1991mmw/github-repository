package com.recruit.githubrepositories.service;

import com.recruit.githubrepositories.assembler.GithubRepositoryAssembler;
import com.recruit.githubrepositories.model.GithubRepositoryPayload;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.time.LocalDate.parse;

@Service
public class GithubRepositoriesService {

    private RestTemplate restTemplate;
    private GithubRepositoryAssembler githubRepositoryAssembler;

    GithubRepositoriesService(RestTemplate restTemplate, GithubRepositoryAssembler githubRepositoryAssembler) {
        this.restTemplate = restTemplate;
        this.githubRepositoryAssembler = githubRepositoryAssembler;
    }

    @Async
    public CompletableFuture<GithubRepositoryPayload> getGithubRepositoryDetails(String owner, String repositoryName) {
        ResponseEntity<String> forEntity = extractGithubRepositoryDetails(owner, repositoryName);
        JSONObject jsonObject = fetchGithubRepositoryJsonObject(forEntity);
        return CompletableFuture.completedFuture(retrieveRepositoryDetails(jsonObject));
    }

    private ResponseEntity<String> extractGithubRepositoryDetails(String owner, String repositoryName) {
        String url = "https://api.github.com/search/repositories?q=repo:" + owner + "/" + repositoryName;
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
        return forEntity;
    }


    private GithubRepositoryPayload retrieveRepositoryDetails(JSONObject jsonObject) {
        Integer stargazers_count = (Integer) jsonObject.get("stargazers_count");
        String full_name = isNull(jsonObject.get("full_name"));
        String description = isNull(jsonObject.get("description"));
        String clone_url = isNull(jsonObject.get("clone_url"));
        String created_at = (String) jsonObject.get("created_at");
        LocalDate dateIsoFormat = parseDateToIsoFormat(created_at);
        return githubRepositoryAssembler.assembleToPayload(full_name, description, clone_url, stargazers_count, dateIsoFormat);
    }

    private String isNull(Object o) {
        if (o.equals(null)) {
            return "N/A";
        } else {
            return (String) o;
        }
    }


    private LocalDate parseDateToIsoFormat(String created_at) {
        return parse(created_at, DateTimeFormatter.ISO_DATE_TIME);
    }

    private JSONObject fetchGithubRepositoryJsonObject(ResponseEntity<String> forEntity) {
        String body = forEntity.getBody();
        JSONObject obj = new JSONObject(body);
        JSONArray items = obj.getJSONArray("items");
        return items.getJSONObject(0);
    }

}
