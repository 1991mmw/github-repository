package com.recruit.githubrepositories.model;
import java.time.LocalDate;

public class GithubRepositoryPayload {

    public static class Builder {

        private String fullName;
        private String description;
        private String cloneUrl;
        private int stars;
        private LocalDate createdAt;

        public Builder withFullName(String fullName) {
            this.fullName = fullName;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withCloneUrl(String cloneUrl) {
            this.cloneUrl = cloneUrl;
            return this;
        }

        public Builder withStarGazers(int stars) {
            this.stars = stars;
            return this;
        }

        public Builder withCreatedAt(LocalDate createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public GithubRepositoryPayload build() {
            GithubRepositoryPayload payload = new GithubRepositoryPayload();
            payload.setFullName(fullName);
            payload.setCloneUrl(cloneUrl);
            payload.setDescription(description);
            payload.setCreatedAt(createdAt);
            payload.setStars(stars);
            return payload;
        }
    }

    private GithubRepositoryPayload() {

    }

    private String fullName;
    private String description;
    private String cloneUrl;
    private int stars;
    private LocalDate createdAt;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCloneUrl() {
        return cloneUrl;
    }

    public void setCloneUrl(String cloneUrl) {
        this.cloneUrl = cloneUrl;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }
}
