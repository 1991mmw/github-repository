package com.recruit.githubrepositories.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class GithubRepositoryNotFoundException extends RuntimeException {

    public GithubRepositoryNotFoundException() {
        super();
    }

    public GithubRepositoryNotFoundException(String owner, String repositoryName) {
        super(String.format("Can't find Github repository for given owner: [%s] and repository name: [%s]", owner, repositoryName));
    }

}
