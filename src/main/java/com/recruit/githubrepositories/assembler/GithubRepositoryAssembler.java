package com.recruit.githubrepositories.assembler;

import com.recruit.githubrepositories.model.GithubRepositoryPayload;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class GithubRepositoryAssembler {

    public GithubRepositoryPayload assembleToPayload(String full_name, String description, String clone_url,
                                                     Integer stargazers_count, LocalDate dateIsoFormat) {
        return new GithubRepositoryPayload.Builder()
                .withCloneUrl(clone_url)
                .withCreatedAt(dateIsoFormat)
                .withDescription(description)
                .withFullName(full_name)
                .withStarGazers(stargazers_count)
                .build();
    }

}
