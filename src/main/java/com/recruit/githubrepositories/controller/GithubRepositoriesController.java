package com.recruit.githubrepositories.controller;

import com.recruit.githubrepositories.model.GithubRepositoryPayload;
import com.recruit.githubrepositories.service.GithubRepositoriesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@Controller
public class GithubRepositoriesController {

    private GithubRepositoriesService service;

    public GithubRepositoriesController(GithubRepositoriesService service) {
        this.service = service;
    }

    @GetMapping(value = "/repositories/{owner}/{repositoryName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GithubRepositoryPayload> getGithubRepositoryDetails(
            @PathVariable("owner") final String owner,
            @PathVariable("repositoryName") final String repositoryName) {
        CompletableFuture<GithubRepositoryPayload> githubRepositoryDetails = service.getGithubRepositoryDetails(owner, repositoryName);
        GithubRepositoryPayload githubRepositoryPayload = null;
        try {
            githubRepositoryPayload = githubRepositoryDetails.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(githubRepositoryPayload, HttpStatus.OK);
    }
}