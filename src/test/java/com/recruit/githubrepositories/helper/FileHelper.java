package com.recruit.githubrepositories.helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class FileHelper {

    public static String readFile(String cityName) {
        File file = getFileFromResources(cityName);
        try {
            return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static File getFileFromResources(String testResponse) {
        ClassLoader classLoader = FileHelper.class.getClassLoader();
        URL resource = classLoader.getResource("response/" + testResponse.toLowerCase() + ".json");
        if(resource != null) {
            return new File(resource.getFile());
        } else {
            throw new RuntimeException();
        }
    }
}