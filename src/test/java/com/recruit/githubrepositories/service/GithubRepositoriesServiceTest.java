package com.recruit.githubrepositories.service;

import com.recruit.githubrepositories.assembler.GithubRepositoryAssembler;
import com.recruit.githubrepositories.model.GithubRepositoryPayload;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.recruit.githubrepositories.helper.FileHelper.readFile;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class GithubRepositoriesServiceTest {

    @Mock
    private RestTemplate restTemplate;
    private GithubRepositoriesService testedService;

    @Before
    public void setUp() {
        testedService = new GithubRepositoriesService(restTemplate, new GithubRepositoryAssembler());
    }

    @Test
    public void shouldGetRepoDetails() throws ExecutionException, InterruptedException {
        //given
        ResponseEntity<String> spy = Mockito.spy(new ResponseEntity<>(readFile("test_response"), HttpStatus.OK));
        doReturn(spy).when(restTemplate).getForEntity(
                any(String.class),
                any(Class.class));
        //when
        String owner = "audial";
        String repositoryName = "GitHub";
        CompletableFuture<GithubRepositoryPayload> githubRepositoryDetails = testedService.getGithubRepositoryDetails(owner, repositoryName);
//        then
        assertThat(githubRepositoryDetails).isNotNull();
        assertThat(githubRepositoryDetails.get().getFullName()).contains(owner);
        assertThat(githubRepositoryDetails.get().getFullName()).contains(repositoryName);
    }
}
