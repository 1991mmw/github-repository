package com.recruit.githubrepositories.controller;

import com.recruit.githubrepositories.exception.GithubRepositoryNotFoundException;
import com.recruit.githubrepositories.model.GithubRepositoryPayload;
import com.recruit.githubrepositories.service.GithubRepositoriesService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {GithubRepositoriesController.class})
public class GithubRepositoriesControllerTest {

    private static final String OWNER = "owner";
    private static final String REPO_NAME = "repositoryName";

    @Autowired
    WebApplicationContext wac;
    @MockBean
    private GithubRepositoriesService service;
    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        mockMvc = webAppContextSetup(wac)
                .build();
    }

    @Test
    public void shouldFetchRepoInformationByNameAndReturnHttpOk() throws Exception {
        //given
        when(service.getGithubRepositoryDetails(OWNER, REPO_NAME)).thenReturn(CompletableFuture.completedFuture(buildPayload()));

        //when

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/repositories/{owner}/{repositoryName}", OWNER, REPO_NAME);
        ResultActions resultActions = mockMvc.perform(requestBuilder);
        //then
        resultActions.andExpect(status().isOk());
    }

    private GithubRepositoryPayload buildPayload() {
        return new GithubRepositoryPayload.Builder().withStarGazers(0).withFullName("owner/repositoryName").withDescription("N/A").withCreatedAt(LocalDate.EPOCH).withCloneUrl("www.site.com").build();
    }

    @Test
    public void shouldNotReturnFetchRepoInformationByNameAndReturnHttpNotFound() throws Exception {
        //given
        when(service.getGithubRepositoryDetails(OWNER, REPO_NAME))
                .thenThrow(GithubRepositoryNotFoundException.class);

        //when
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/repositories/{owner}/{repositoryName}", OWNER, REPO_NAME);
        ResultActions resultActions = mockMvc.perform(requestBuilder);
        //then
        resultActions.andExpect(status().isNotFound());
    }

}