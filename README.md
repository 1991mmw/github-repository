# Github Repositories

Github Repositories is a Java REST API for finding details of a particular github repository.

## Installation

Use Maven 3

## Usage

```
GET /repositories/{owner}/{repositories}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.